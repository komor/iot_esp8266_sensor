#include <Arduino.h>
#include <AsyncMqttClient.h>
#include <ESP8266WiFi.h>
#include "DHT.h"
#include "id.h"

#define WIFI_SSID "xxxxx"
#define WIFI_PASSWORD "xxxxx"

#define MQTT_HOST IPAddress(192, 168, 1, 48)
#define MQTT_PORT 1883

#define DHT_PIN 2

#define TOPIC_IDS "ids"

#define printf Serial.printf


#define TOPIC_PUB_HEREIAM "sensors/avaible"
#define TOPIC_PREFIX "sensors/"ID
#define TOPIC_SUB_MYTOPICS TOPIC_PREFIX"/show_topics"

#define TOPIC_PUB_MYTOPICS TOPIC_PREFIX"/topics"

#define TOPIC_SUB_YOURID "sensors/askall"

#define TOPIC_PUB_TEMPERATURE "temperature"
#define TOPIC_PUB_HUMIDITY "humidity"
#define TOPIC_PUB_PREASURE "preasure"

#define MY_TOPICS TOPIC_PREFIX"/"TOPIC_PUB_TEMPERATURE" "TOPIC_PREFIX"/"TOPIC_PUB_HUMIDITY


AsyncMqttClient mqttClient;
DHT dht;


void connectToWifi()
{
    printf("Connecting to Wi-Fi...\n\r");
    
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void connectToMqtt()
{
    printf("Connecting to MQTT...\n\r");
    
    mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event)
{
    printf("[WiFi-event] event: %d\n\r", event);

    switch (event)
    {
    case WIFI_EVENT_STAMODE_GOT_IP:

        printf("WiFi connected\n\r");
        Serial.println(WiFi.localIP());

        connectToMqtt();

        break;

    case WIFI_EVENT_STAMODE_DISCONNECTED:

        printf("WiFi lost connection\n\r");
        // xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
        //   xTimerStart(wifiReconnectTimer, 0);
        break;
    }
}

void onMqttConnect(bool sessionPresent)
{
    printf("Connected to MQTT.\n\r");
    printf("Session present: %d\n\r", sessionPresent);

    // uint16_t packetIdSub = mqttClient.subscribe("test/lol", 2);
    // mqttClient.publish(TOPIC_IDS, 0, true, ID);

    //  xTimerStart(measureTimer, 0);

    mqttClient.publish(TOPIC_PUB_HEREIAM, 0, true, ID);
    mqttClient.subscribe(TOPIC_SUB_YOURID, 2);
    mqttClient.subscribe(TOPIC_SUB_MYTOPICS, 2);

}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos)
{
    printf("Subscribe acknowledged.\n\r");
    printf("  packetId: %d\n\r", packetId);
    printf("  qos: %d\n\r", qos);



}

void onMqttUnsubscribe(uint16_t packetId)
{
    printf("Unsubscribe acknowledged.\n\r");
    printf("  packetId: %d\n\r", packetId);
}

void onMqttMessage(char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total)
{
    printf("Publish received. %s\n\r", topic);

    printf("  qos: %d\n\r", properties.qos);
    printf("  dup: %d\n\r", properties.dup);

    if(String(topic) == TOPIC_SUB_YOURID){

        mqttClient.publish(TOPIC_PUB_HEREIAM, 0, true, ID);

    } else if (String(topic) == TOPIC_SUB_MYTOPICS) {
        
        mqttClient.publish(TOPIC_PUB_MYTOPICS, 0, true, MY_TOPICS);
    }
    else {

    }
}

void onMqttPublish(uint16_t packetId)
{
    printf("Publish acknowledged.\n\r");
    printf("  packetId: %d\n\r", packetId);

}

void makeMeasure()
{
    Serial.print("making measure\n\r");
    float temp = dht.getTemperature();
    float hum = dht.getHumidity();

    char temp_s[10] = {0};
    char hum_s[10] = {0};

    snprintf(temp_s, 10, "%d", (int) temp);
    snprintf(hum_s, 10, "%d", (int)  hum);

    printf("Temp: %d\n\r", (int) temp);
    printf("Hum: %d\n\r", (int) hum);

    mqttClient.publish(TOPIC_PREFIX"/"TOPIC_PUB_TEMPERATURE, 0, true, temp_s);
    mqttClient.publish(TOPIC_PREFIX"/"TOPIC_PUB_HUMIDITY, 0, true, hum_s);

}


void setup()
{

    Serial.begin(9600);
    printf("Init\n\r");

    WiFi.onEvent(WiFiEvent);

    pinMode(DHT_PIN, INPUT_PULLUP);

    dht.setup(DHT_PIN);

    mqttClient.onConnect(onMqttConnect);
    mqttClient.onDisconnect(onMqttDisconnect);
    mqttClient.onSubscribe(onMqttSubscribe);
    mqttClient.onUnsubscribe(onMqttUnsubscribe);
    mqttClient.onMessage(onMqttMessage);
    mqttClient.onPublish(onMqttPublish);
    mqttClient.setServer(MQTT_HOST, MQTT_PORT);

    connectToWifi();

    printf(TOPIC_SUB_MYTOPICS);
    printf(MY_TOPICS);

    //timer = timer_create_default();
    
}

void loop()
{

    makeMeasure();
    delay(2000);
}
